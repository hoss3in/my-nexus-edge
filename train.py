# coding=utf-8
"""
The traning module for the Nexus Edge Kickstarter coding chanllenge
This is the final ML model, so there is not testing in this module.
For furture information on how I have came up with the features and
the trained model please refer to the hossein_challenge.ipynb

you can run this module by typing the following command in the terminal's command line:

train.py -t ./data.csv -p ./unknown.csv

the first aurgument is the path to training CSV file,
while the second aurgument is the path to the prediction file
"""

import sys
import getopt
import logging
import pandas as pd
import numpy as np
from datetime import datetime
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression


logging.basicConfig(filename='info.log', level=logging.INFO)


def _data_processor(filepath, training=True):
    """
    Method to import and process the dataset and get it ready for training
    :param filepath: the local path to the CSV dataset
    :type filepath: str
    :param training: True if this dataset is for training and has labels
    :type training: bool
    :return: two dataframes the features and the label
    :rtype: tuple
    """
    assert (filepath.endswith('.csv'))
    dataset = pd.read_csv(filepath)

    # refining the labels to be binary 0 or 1
    le = None
    if training:
        dataset = dataset[(dataset['state'] != 'live') & (dataset['state'] != 'undefined')]
        dataset['state'] = np.where(dataset['state'] == 'failed', 'unsuccessful', dataset['state'])
        dataset['state'] = np.where(dataset['state'] == 'canceled', 'unsuccessful', dataset['state'])
        dataset['state'] = np.where(dataset['state'] == 'suspended', 'unsuccessful', dataset['state'])

        le = preprocessing.LabelEncoder()
        le.fit(dataset['state'].values)
        dataset['Label'] = le.transform(dataset['state'])
        logging.info('Created encoded labels 0 means {0} and 1 means {1}'.format(le.inverse_transform([0])[0],
                                                                                 le.inverse_transform([1])[0]))

    # feature engineering: selecting and creating the proper features
    dataset['#Days'] = dataset.apply(lambda row: (datetime.strptime(row['deadline'], '%Y-%m-%d').date() - datetime.
                                                  strptime(row['launched'].split(' ')[0], '%Y-%m-%d').date()).days,
                                     axis=1)
    dataset['Year'] = dataset.apply(lambda row: datetime.strptime(row['launched'].split(' ')[0], '%Y-%m-%d').date()
                                    .year, axis=1)
    dataset['PledgeRatio'] = dataset.apply(lambda row: row['usd_pledged_real'] / row['usd_goal_real'], axis=1)
    logging.info('Created "PledgeRatio", "Year" and "#Days" as new features.')

    # Trimming the dataset
    features = ['backers', 'PledgeRatio', '#Days', 'Year']
    dataset = dataset[dataset['PledgeRatio'] < 90]
    dataset = dataset[dataset['Year'] > 2000]
    dataset = dataset[dataset['backers'] < 10000]
    logging.info(dataset[features].describe())

    ce = preprocessing.LabelEncoder()
    ce.fit(dataset['main_category'].values)
    cat_dummy = pd.get_dummies(dataset['main_category'], prefix='cat')
    data = dataset.join(cat_dummy)
    feature_list = ['backers', 'PledgeRatio', '#Days', 'Year'] + ['cat_' + i for i in ce.classes_]
    if training:
        feature_list.extend(['Label'])
    data = data[feature_list]

    data_final_vars = data.columns.values.tolist()
    if training:
        y = data['Label']
    else:
        y = None
    x = data[[i for i in data_final_vars if i not in ['Label']]]
    return x, y, le


def train(filepath):
    """
    Method to train the model based on the dataset x and its labels y
    :param filepath: the path to the file that you want to predict
    :type filepath: str
    :return: the trained model
    rtype: sklearn.linear_model.logistic.LogisticRegression object
    """
    x, y, le = _data_processor(filepath)
    logreg = LogisticRegression()
    logreg.fit(x, y)
    logging.info('Done with training')
    return logreg, le


def predict(model, filepath, label_encoder):
    """
    Method to predict the label based on the inputs and writes the output to the prediction.xlsx file
    :param model: the trained model from the 'train' method
    :type model: sklearn.linear_model.logistic.LogisticRegression object
    :param filepath: the path to the file that you want to predict
    :type filepath: str
    :param label_encoder: the object for decoding the labels
    :return: None
    """
    x, _, _ = _data_processor(filepath, training=False)
    y = model.predict(x)
    writer = pd.ExcelWriter('prediction.xlsx', engine='xlsxwriter')
    x['state'] = label_encoder.inverse_transform(y)
    x.to_excel(writer)


def main(argv):
    """
    The main method to run the module
    :param argv: the train and predict file paths
    :return: None
    """
    logging.info('Starting a new run at {0}'.format(datetime.now()))
    training_file = ''
    predict_file = ''
    try:
        opts, args = getopt.getopt(argv, "ht:p:", ["tfile=", "pfile="])
    except getopt.GetoptError:
        print('Please pass the proper aurguments:\n train.py -t <trainfile> -p <predictfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('train.py -t <trainfile> -p <predictfile>')
            sys.exit()
        elif opt in ("-t", "--tfile"):
            training_file = arg
        elif opt in ("-p", "--pfile"):
            predict_file = arg
    training_file = training_file or './data.csv'
    if not predict_file:
        logging.warning('No predict file was given. Using training file as predict file!')
        predict_file = training_file

    model, le = train(training_file)
    predict(model, predict_file, le)


if __name__ == "__main__":
    main(sys.argv[1:])
